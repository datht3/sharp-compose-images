const sharp = require('sharp')
const path = require('path')

const DIR = path.join(__dirname, 'images')

const backgroundPath = path.join(DIR, 'background.png')
const outPath = path.join(DIR, 'out.png')

const images = [
    {
        input: path.join(DIR, '1.png'),
        top: 283,
        left: 222,
    },
    {
        input: path.join(DIR, '2.png'),
        top: 301,
        left: 4868,
    },
    {
        input: path.join(DIR, '3.png'),
        top: 301,
        left: 6615,
    },
    {
        input: path.join(DIR, '4.png'),
        top: 80,
        left: 9219,
    },
    {
        input: path.join(DIR, '5.png'),
        top: 1983,
        left: 9219,
    },
    {
        input: path.join(DIR, '6.png'),
        top: 5492,
        left: 0,
    },    
]

async function main() {
    console.time('compose images')
    await sharp(backgroundPath).composite(images).png().toFile(outPath)
    console.timeEnd('compose images')
}

main()